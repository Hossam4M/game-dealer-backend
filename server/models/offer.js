module.exports = function (Offer) {
  Offer.beforeRemote("create", async (ctx) => {
    ctx.req.body.ownerId = ctx.req.body.ownerId || ctx.req.accessToken.userId;
  });
};
