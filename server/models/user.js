module.exports = function (User) {
  User.validatesUniquenessOf("phone", {
    message: "Phone number already exists ",
  });
};
