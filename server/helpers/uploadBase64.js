const uuid1 = require("uuid/v1");
const fileType = require("file-type");

const app = require("../server");
const { compress: compressImage } = require("../helpers/optimizeImages");

const encodedFileContainer = "attachments";

function isImage(ext) {
  return ["jpg", "JPG", "jpeg", "JPEG", "png", "svg", "gif"].includes(ext);
}

function uploadBase64(encodedFile, originalName, ext = "") {
  const fileName = originalName || uuid1() + (ext && `.${ext}`);
  const uploadStream = app.models.container.uploadStream(
    encodedFileContainer,
    fileName
  );
  return new Promise((resolve, reject) => {
    uploadStream.end(encodedFile, "base64", (err) => {
      if (err) return reject(err);
      return resolve(fileName);
    });
  });
}

async function convertBase64ToURL(file, originalName) {
  const CONTAINERS_URL = "/api/containers/";
  if (file.includes(",")) {
    [, file] = file.split(",");
  }

  const fileObj = fileType(Buffer.from(file, "base64"));
  const storedFileName = await uploadBase64(file, originalName, fileObj.ext);
  const [fileName] = storedFileName.split(".");

  const isImageExtension = isImage(fileObj.ext);
  if (isImageExtension) compressImage();

  // eslint-disable-next-line consistent-return
  return {
    url: `${CONTAINERS_URL}${encodedFileContainer}/download/${storedFileName}`,
    optimizedUrl:
      isImageExtension &&
      `${CONTAINERS_URL}${encodedFileContainer}/download/${fileName}.webp`,
  };
}

module.exports = {
  convertBase64ToURL,
};
