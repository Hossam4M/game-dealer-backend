async function runPromisesSequentially(asyncFunctionsArr, args) {
  return asyncFunctionsArr.reduce(
    async (asyncFn, nextPromise) => asyncFn.then((v) => nextPromise(args)),
    Promise.resolve()
  );
}

module.exports = {
  runPromisesSequentially,
};
